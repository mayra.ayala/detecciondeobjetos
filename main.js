
//-------------------------------- DEclaracion de canvas donde se mostara el video --------------------------

let fullimage = document.getElementById('CanvasFHD')
let fullimagectx = fullimage.getContext('2d')

let recorte = document.getElementById('recorte')
let recortectx = recorte.getContext('2d')

//--------------------------------------------- Soporte de navegacion ----------------------------------------
/*function getUserMediaSupported() {
    return !!(navigator.mediaDevices &&
      navigator.mediaDevices.getUserMedia);
  }*/
//--------------------------------------------- Apertura de camara -------------------------------------------
async function open_cam(){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    camid="16166c7b1d725d5156b8502a6edf3f3b5d30272d1917012ed3bf75c43967d4d0"// camara 1

    const video = document.querySelector('video');
     const vgaConstraints = {
           
        video: {deviceId: camid}, //width: {exact: 280}, height: {exact: 280} / deviceId: "5bba2c7c9238e1d8ab5e90e2f2f94aa226749826319f6c705c5bfb5a3d2d5279"
                 width: { ideal: 1920 },
                 height: { ideal: 1080 }
        };

         await navigator.mediaDevices.getUserMedia(vgaConstraints).
        then((stream) => {video.srcObject = stream});
        setTimeout(function fire(){resolve('resolved');},2000);
    });//Cierra Promise principal
}
//--------------------------------------------- busqueda de id's por camara detectada--------------------------
function mapcams(){ // Mapeo de camaras para identificarlas 
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}

function captureimage() {// Resolve de 2 segundos
    return new Promise(async resolve => {

        let image = document.getElementById('CanvasFHD');
        let contexim2 = image.getContext('2d');

        var video = document.getElementById("video");

        w = image.width;
        h = image.height;

        contexim2.drawImage(video, 0, 0, image.width, image.height);
        //var dataURI = canvas.toDataURL('image/jpeg');
        //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        resolve('resolved')
    });
}

//----------------------------------------------- IA ----------------------------------------------------------
let model = new cvstfjs.ObjectDetectionModel();

async function loadmodel(){

    await model.loadModelAsync('../neural-network2/model.json');
    console.log("Modelo cargado...")
    const video = document.getElementById('video');
    const result = await model.executeAsync(video)

    /*const image = document.getElementById('image');
    const result = await model.executeAsync(image)*/
    console.log(result)
    console.log(result[0])
    let firstElement = result[0][0]
    console.log(firstElement)//Primer elemento del array de arrays 
    //let predictions = result[1][0]
    //console.log("Soy predictions", predictions)//Primer valor de porcentaje de confiabilidad 
    let etiqueta = result[2][0]
    console.log(etiqueta)//Ubicacion de primer etiqueta 
    //await results(predictions,result,etiqueta)
    let predictions = {
        bbox: firstElement,
        class: "objeto",
        score: 0.6385624408721924
    }
    await predictWebcam(predictions)
    console.log(predictions)
}

async function predict(canvaswork){
    let fullimage = document.getElementById( 'CanvasFHD' )
    let input_size = model.input_size

    // Take & Pre-process the image
	let image = tf.browser.fromPixels(canvaswork, 3) // se utiliza para crear un tensor de valores de píxeles de una imagen específica. 
	image = tf.image.resizeBilinear(image.expandDims(),[input_size, input_size]) //  
	let predictions = await model.executeAsync(image)
	
    console.log(input_size)
	console.log(predictions)
    predictWebcam(predictions)
}

async function results(predictions,result,etiqueta){
    if (predictions > 0.29) { 
        console.log("falle: "+predictions)
        let videoElement = document.getElementById("video")
        let nuevoParrafo = document.createElement("p")
        nuevoParrafo.innerText = clases[result[2][0]] + ': '
            + Math.round(parseFloat(predictions) * 100)
            + '%';
           // console.log(p)
           miVideoContainer.appendChild(nuevoParrafo)
    }else if(predictions < 0.29){
        console.log("menor al valor ")
        let videoElement = document.getElementById("video")
        let nuevoParrafo = document.createElement("p")
        nuevoParrafo.innerText = clases[result[2][0]] + ': '
            + Math.round(parseFloat(predictions) * 100)
            + '%';
            //console.log(p)
            miVideoContainer.appendChild(nuevoParrafo)
    }

}

var children = [];
// Placeholder function for next step. 
async function predictWebcam(predictions) { // funcion que contiene el proceso del reconocimento 
    console.log("Estoy en funcion predictwebcam")
    console.log(predictions)
    // Now let's start classifying a frame in the stream.      // Empezar a clasificar un cuadro en la secuencia 
    //model.detect(video).then(function (predictions) { 
    // Remove any highlighting we did previous frame.     // Elimine cualquier resaltado que hicimos en el cuadro anterior.
   /*for (let i = 0; i < children.length; i++) {
    liveView.removeChild(children[i]);
  }
  children.splice(0); //cambia el contenido de un array eliminando elementos existentes y/o agregando nuevos elementos
  */
  // Now lets loop through predictions and draw them to the live view if
  // they have a high confidence score.
  //for (let n = 0; n < predictions.length; n++) {
    // If we are over 66% sure we are sure we classified it right, draw it!
    if (predictions.score > 0.06) {
        let videoElement = document.getElementById("video")
        let nuevoParrafo = document.createElement("p")
      //const p = document.createElement('p');
      nuevoParrafo.innerText= predictions.class  + ' - with ' 
          + Math.round(parseFloat(predictions.score) * 100) 
          + '% confidence.';
      nuevoParrafo.style = 'margin-left: ' 
          + predictions.bbox[0] * 640 + 'px; margin-top: '
          + (predictions.bbox[1] * 480 ) + 'px; width: ' 
          + (predictions.bbox[2] * 640) + 'px; top: 0; left: 0;';
          console.log(predictions)
      const highlighter = document.createElement('div');
      highlighter.setAttribute('class', 'highlighter');
      highlighter.style = 'left: '
          + predictions.bbox[0] * 640 + 'px; top: '
          + predictions.bbox[1] * 480 + 'px; width: ' 
          + predictions.bbox[2] * 640 + 'px; height: '
          + predictions.bbox[3] * 480 + 'px;';
        
      miVideoContainer.appendChild(highlighter);
      miVideoContainer.appendChild(nuevoParrafo);
      children.push(highlighter);
      children.push(nuevoParrafo);
    }
  //}
    //Call this function again to keep predicting when the browser is ready.
   //window.requestAnimationFrame(predictWebcam);
}