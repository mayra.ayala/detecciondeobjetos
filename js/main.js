
//-------------------------------- DEclaracion de canvas donde se mostara el video --------------------------
/*
let fullimage = document.getElementById('CanvasFHD')
let fullimagectx = fullimage.getContext('2d')*/



//--------------------------------------------- Soporte de navegacion ----------------------------------------
/*function getUserMediaSupported() {
    return !!(navigator.mediaDevices &&
      navigator.mediaDevices.getUserMedia);
  }*/
//--------------------------------------------- Apertura de camara -------------------------------------------
async function open_cam(){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    camid="16166c7b1d725d5156b8502a6edf3f3b5d30272d1917012ed3bf75c43967d4d0"// camara 1

    const video = document.querySelector('video');
     const vgaConstraints = {
           
        video: {deviceId: camid}, //width: {exact: 280}, height: {exact: 280} / deviceId: "5bba2c7c9238e1d8ab5e90e2f2f94aa226749826319f6c705c5bfb5a3d2d5279"
                 width: { ideal: 1920 },
                 height: { ideal: 1080 }
        };

         await navigator.mediaDevices.getUserMedia(vgaConstraints).
        then((stream) => {video.srcObject = stream});
        setTimeout(function fire(){resolve('resolved');},2000);
    });//Cierra Promise principal
}
//--------------------------------------------- busqueda de id's por camara detectada--------------------------
function mapcams(){ // Mapeo de camaras para identificarlas 
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}

function captureimage() {// Resolve de 2 segundos
    return new Promise(async resolve => {

        let image = document.getElementById('CanvasFHD');
        let contexim2 = image.getContext('2d');

        var video = document.getElementById("video");

        w = image.width;
        h = image.height;

        contexim2.drawImage(video, 0, 0, image.width, image.height);
        //var dataURI = canvas.toDataURL('image/jpeg');
        //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        resolve('resolved')
    });
}


async function detect(){

    let model = new cvstfjs.ObjectDetectionModel();
    await model.loadModelAsync('model.json');
    const image = document.getElementById('image');
    const result = await model.executeAsync(image);
    console.log(result)
}
